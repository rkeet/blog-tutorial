ZF2 + Doctrine2 ORM - Creating a basic blog
=======================

Introduction
------------
Using the [Zend Framework 2 Skeleton Application](https://github.com/zendframework/ZendSkeletonApplication) and [Doctrine2 ORM](http://www.doctrine-project.org/projects/orm.html) I've created a simple blog application.

This tutorial uses [Zend Skeleton Application release 2.5](https://github.com/zendframework/ZendSkeletonApplication/tree/release-2.5.0) (with Zend Framework ~2.5) and Doctrine 2 version ~1.0. Make sure you use correct releases when downloading them. 

The application makes use of configuration set globally, locally and per module to be able to run everything contained into their own module folders.

To install this project for yourself, download this repository, make sure you have Composer and Git Bash installed (instructions below) and run the Composer install command in your Terminal.

In your php.ini file make sure to have the extension "intl" enabled and to have set your local timezone.

Installing Composer
---------------------------

The easiest way to create a new ZF2 project is to use [Composer](https://getcomposer.org/). If you don't have it already installed, then please install as per the [documentation](https://getcomposer.org/doc/00-intro.md).

Configuration of OS that created tutorial
---------------------------
### Software
* Windows 10
* Xampp
* PhpStorm
* Git Bash

### Windows setup

Add the following line to your 'hosts' file, located in C:/Windows/System32/drivers/etc (note: will require admin privileges to overwrite)

    127.0.0.1	blog.loc

### Apache setup

To setup apache, setup a virtual host to point to the root directory of the project and you should be ready to go! It should look something like below:

    <VirtualHost *:80>
        DocumentRoot "C:/xampp/htdocs/blog"
        ServerName blog.loc
        ErrorLog "logs/blog.loc-error.log"
        CustomLog "logs/blog.loc-access.log" common

        SetEnv APPLICATION_ENV "development"
    </VirtualHost>

If you're using XAMPP this file by default will be located at: C:/xampp/apache/conf/extra/httpd-vhosts.conf

### Database setup

By now I'm assuming you've installed something like XAMPP (or WAMP/LAMP/etc) and you've also gotten with it PhpMyAdmin.

Setup the database for this tutorial application by creating a user, setting the following options:

* Username: blog
* Password: blog
* Hostname: localhost
* Option "Create database with same name and grant all privileges": check option

When you've created the user and database, you can "enter" the database, go to the SQL or Import tab and execute the MySQL code located in the root folder file: blog.sql. This creates a demo set of data to illustrate the results of this tutorial.