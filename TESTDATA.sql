CREATE DATABASE IF NOT EXISTS `tutorials` DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci;
USE `tutorials`;

CREATE TABLE `posts` (
`id` int(11) NOT NULL,
`title` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
`slug` varchar(128) COLLATE utf8_unicode_ci NOT NULL COMMENT 'Slug used in URL',
`created` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
`body` text COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO `posts` (`id`, `title`, `slug`, `created`, `body`) VALUES
(1, 'First post', 'first_post', '2016-01-02 17:47:18', 'This is the first post to have in tutorial'),
(2, 'Second post', 'second_post', '2016-01-02 17:47:18', 'This is the second post to have in tutorial'),
(3, 'Third post', 'third_post', '2016-01-02 17:47:18', 'This is the third post post to have in tutorial'),
(4, 'Fourth post', 'fourth_post', '2016-01-02 17:47:18', 'This is the fourth post to have in tutorial'),
(5, 'Fifth post', 'fifth_post', '2016-01-02 17:47:18', 'This is the fifth post to have in tutorial');

ALTER TABLE `posts`
ADD PRIMARY KEY (`id`),
ADD UNIQUE KEY `AK_posts_slug` (`slug`) USING BTREE;

ALTER TABLE `posts`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;