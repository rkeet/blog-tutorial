<?php return [
    'db' => [
        'driver' => 'Pdo',
        'dsn'    => 'mysql:dbname=blog;host=localhost',
        'driver_options' => [
            PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES \'UTF8\'',
        ],
    ],
    'doctrine'           => [
        'connection'    => [
            // default connection name
            'orm_default' => [
                'driverClass' => 'Doctrine\DBAL\Driver\PDOMySql\Driver',
            ],
        ],
        'entitymanager' => [
            'orm_default' => [
                'connection'    => 'orm_default',
                'configuration' => 'orm_default',
            ],
        ],
    ],
    'phpSettings' => [
        'date.timezone' => 'Europe/Amsterdam',
    ],
];