<?php

namespace Application\Form;

use Zend\Di\ServiceLocator;
use Zend\Form\Element\Csrf;
use Zend\Form\ElementInterface;
use Zend\Form\Exception;
use Zend\Form\Exception\InvalidElementException;

class AbstractForm extends \Zend\Form\Form
{
    /**
     * CSRF timeout in seconds
     */
    protected $csrfTimeout = 7200; // 2 hours

    /**
     * @var ServiceLocator
     */
    protected $serviceLocator;

    /**
     * {@inheritDoc}
     */
    public function __construct($name = null, $options = [])
    {
        if (isset($options['serviceLocator'])) {
            $this->serviceLocator = $options['serviceLocator'];
            unset($options['serviceLocator']);
        }

        $csrfName = null;
        if (isset($options['csrfCorrector'])) {
            $csrfName = $options['csrfCorrector'];
            unset($options['csrfCorrector']);
        }

        parent::__construct($name, $options);

        if (null === $csrfName) {
            $csrfName = 'csrf';
        }

        $this->addElementCsrf($csrfName);
    }

    /**
     * Retrieve a named element or fieldset
     *
     * Extends Zend\Form with CSRF fields that can be retrieved by the name "CSRF"
     * but are resolved to their unique name
     *
     * @param  string                  $elementOrFieldset
     * @throws InvalidElementException
     * @return ElementInterface
     */
    public function get($elementOrFieldset)
    {
        if ($elementOrFieldset === 'csrf') {
            // Find CSRF element
            foreach ($this->elements as $formElement) {
                if ($formElement instanceof Csrf) {
                    return $formElement;
                }
            }
        } else {
            return parent::get($elementOrFieldset);
        }
    }

    /**
     * Adds CSRF protection
     */
    protected function addElementCsrf($csrfName = 'csrf')
    {
        // Set unique CSRF name based on module name, form name and csrfName variable to avoid CSRF collisions
        // on multiple forms in different browser tabs
        $className = get_class($this);
        $parts = explode('\\', $className);
        $uniqueCsrfName = lcfirst($parts[0]);
        if (count($parts) > 1) {
            $lastPart = lcfirst($parts[count($parts) - 1]);
            $lastPart = substr($lastPart, 0, strripos($lastPart, 'Form'));
            $uniqueCsrfName .= '_' . $lastPart;
        }
        $uniqueCsrfName .= '_' . $csrfName;

        $this->add([
            'type'    => 'Zend\Form\Element\Csrf',
            'name'    => $uniqueCsrfName,
            'options' => [
                'csrf_options' => [
                    'timeout' => $this->csrfTimeout,
                ],
            ],
        ]);
    }
}