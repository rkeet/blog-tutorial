<?php

namespace Blog;

use Application\Doctrine\ConnectionString;
use Blog\Entity\Post;
use Blog\Form\PostDeleteForm;
use Blog\Form\PostForm;
use Blog\InputFilter\PostInputFilter;
use DoctrineModule\Stdlib\Hydrator\DoctrineObject;
use Zend\InputFilter\InputFilter;
use Zend\Stdlib\ArrayUtils;

class Module
{
    public function getConfig()
    {
        $config = [];

        $configFiles = [
            __DIR__ . '/config/module.config.php',
            __DIR__ . '/config/routes.config.php',
        ];

        // Merge all module config options
        foreach ($configFiles as $configFile) {
            $config = ArrayUtils::merge($config, include $configFile);
        }

        return $config;
    }

    public function getAutoloaderConfig()
    {
        return [
            'Zend\Loader\StandardAutoloader' => [
                'namespaces' => [
                    __NAMESPACE__ => __DIR__ . '/src/' . __NAMESPACE__,
                ],
            ],
        ];
    }

    public function getServiceConfig()
    {
        return [
            'factories' => [
                'post_form' => function ($sm)
                {
                    $form = new PostForm();
                    $form->setInputFilter(new PostInputFilter());

                    //Set Doctrine Object as Hydrator
                    $form->setHydrator(new DoctrineObject($sm->get(ConnectionString::DOCTRINE_DEFAULT)));
                    //Set Doctrine Entity
                    $form->setObject(new Post);

                    return $form;
                },
                'post_delete_form' => function ()
                {
                    $form = new PostDeleteForm();
                    $form->setInputFilter(new InputFilter());

                    return $form;
                }
            ],
        ];
    }
}