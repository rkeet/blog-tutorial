<?php return [
    'router' => [
        'routes' => [
            //routeName: blog
            //route: /blog
            'blog' => [
                'type'          => 'Literal',
                'may_terminate' => true,
                'options'       => [
                    'route'    => '/blog',
                    'defaults' => [
                        'module'     => 'Blog',
                        'controller' => 'Blog\\Controller\\Post',
                        'action'     => 'index',
                    ],
                ],
                'child_routes' => [
                    //routeName: blog/view
                    //route: /blog/:id[/slug]
                    'view' => [
                        'type'          => 'Segment',
                        'may_terminate' => true,
                        'options'       => [
                            'route'       => '/:id[/:slug]',
                            'constraints' => [
                                'id'   => '[a-f0-9]+',
                                'slug' => '[a-zA-Z0-9_-]+',
                            ],
                            'defaults'    => [
                                'module'     => 'Blog',
                                'controller' => 'Blog\\Controller\\Post',
                                'action'     => 'view',
                            ],
                        ],
                        'child_routes' => [
                            //routeName: blog/view/edit
                            //route: /blog/:id[/slug]/edit
                            'edit' => [
                                'type'          => 'Literal',
                                'may_terminate' => true,
                                'options'       => [
                                    'route'    => '/edit',
                                    'defaults' => [
                                        'module'     => 'Blog',
                                        'controller' => 'Blog\\Controller\\Post',
                                        'action'     => 'edit',
                                    ],
                                ],
                            ],
                            'delete' => [
                                //routeName: blog/view/delete
                                //route: /blog/:id[/slug]/delete
                                'type'          => 'Literal',
                                'may_terminate' => true,
                                'options'       => [
                                    'route'    => '/delete',
                                    'defaults' => [
                                        'module'     => 'Blog',
                                        'controller' => 'Blog\\Controller\\Post',
                                        'action'     => 'delete',
                                    ],
                                ],
                            ],
                        ],
                    ],
                    'add' => [
                        //routeName: blog/add
                        //route: /blog/add
                        'type'          => 'Literal',
                        'may_terminate' => true,
                        'options'       => [
                            'route'    => '/add',
                            'defaults' => [
                                'module'     => 'Blog',
                                'controller' => 'Blog\\Controller\\Post',
                                'action'     => 'add',
                            ],
                        ],
                    ],
                ],
            ],
        ],
    ],
];