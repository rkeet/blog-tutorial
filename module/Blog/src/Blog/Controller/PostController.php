<?php

namespace Blog\Controller;

use Application\Entity\Entity;
use Application\Mvc\Controller\AbstractActionController;
use Blog\Entity\Post;
use Blog\Form\PostForm;
use Zend\Http\Request;

class PostController extends AbstractActionController
{
    public function indexAction()
    {
        return [
            'posts' => $this->getEntityManager()->getRepository(Entity::POST)->findAll(),
        ];
    }

    public function viewAction()
    {
        $id = $this->params()->fromRoute('id');

        if (empty($id)) {
            throw new \Exception('Id must be given; if given, it was not found.');
        }

        /** @var Post $post */
        $post = $this->getEntityManager()->getRepository(Entity::POST)->find($id);
        if (!$post instanceof Post) {
            throw new \Exception('Something went wrong retrieving this post, please try again or contact us.');
        }

        return [
            'post' => $post,
        ];
    }

    public function addAction()
    {
        /** @var PostForm $form */
        $form = $this->getServiceLocator()->get('post_form');

        /** @var Request $request */
        $request = $this->getRequest();
        if ($request->isPost()) {
            $form->setData($request->getPost());

            if ($form->isValid()) {
                /** @var Post $post */
                $post = $form->getObject();
                $post->generateSlug();

                //Save post to DB
                $this->getEntityManager()->persist($post);
                $this->getEntityManager()->flush();

                //Redirect user to view new post
                $this->redirect()->toRoute('blog/view', [
                    'id'   => $post->getId(),
                    'slug' => $post->getSlug(),
                ]);
            }
        }

        return [
            'form' => $form,
        ];
    }

    public function editAction()
    {
        $id = $this->params()->fromRoute('id');
        if (empty($id)) {
            throw new \Exception('Id must be given; if given, it was not found.');
        }

        /** @var Post $post */
        $post = $this->getEntityManager()->getRepository(Entity::POST)->find($id);
        if (!$post instanceof Post) {
            throw new \Exception('Something went wrong retrieving this post, please try again or contact us.');
        }

        /** @var PostForm $form */
        $form = $this->getServiceLocator()->get('post_form');
        $form->get('submit')->setValue('Update post');

        /** @var Request $request */
        $request = $this->getRequest();
        if ($request->isPost()) {
            $form->setData($request->getPost());

            if ($form->isValid()) {
                /** @var Post $post */
                $post = $form->getObject();
                $post->generateSlug();

                //Save post to DB
                $this->getEntityManager()->persist($post);
                $this->getEntityManager()->flush();

                //Redirect user to view new post
                $this->redirect()->toRoute('blog/view', [
                    'id'   => $post->getId(),
                    'slug' => $post->getSlug(),
                ]);
            }
        } else {
            // Bind object so existing values are set when viewing form, but only before posting updated post
            $form->bind($post);
        }

        return [
            'form' => $form,
        ];
    }

    public function deleteAction()
    {
        $id = $this->params()->fromRoute('id');
        if (empty($id)) {
            throw new \Exception('Id must be given; if given, it was not found.');
        }

        /** @var Post $post */
        $post = $this->getEntityManager()->getRepository(Entity::POST)->find($id);
        if (!$post instanceof Post) {
            throw new \Exception('Something went wrong retreving this post, please try again or contact us.');
        }

        /** @var PostForm $form */
        $form = $this->getServiceLocator()->get('post_delete_form');

        /** @var Request $request */
        $request = $this->getRequest();
        if ($request->isPost()) {
            $form->setData($request->getPost());

            if ($form->isValid()) {
                if (array_key_exists('yes', $request->getPost()->toArray()['submit'])) {
                    //Save post to DB
                    $this->getEntityManager()->remove($post);
                    $this->getEntityManager()->flush();

                    //Redirect user to view all posts
                    $this->redirect()->toRoute('blog');
                } else {

                    return $this->redirect()->toRoute('blog/view', [
                        'id' => $post->getId(),
                        'slug' => $post->getSlug(),
                    ]);
                }
            }
        }

        // Bind object so existing values are set when viewing form
        $form->bind($post);
        return [
            'form' => $form,
        ];
    }
}