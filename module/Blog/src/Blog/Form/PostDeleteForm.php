<?php
namespace Blog\Form;

use Application\Form\AbstractForm;
use Zend\Form\Element;

class PostDeleteForm extends AbstractForm
{
    public function __construct($name = 'post-delete-form', $options = [])
    {
        parent::__construct($name, $options);

        $this->addElements();
    }

    public function addElements()
    {
        $this->add([
            'name'       => 'submit[yes]',
            'type'       => 'submit',
            'attributes' => [
                'name' => 'submit[yes]',
                'value'       => 'Delete post',
                'data-action' => 'delete',
            ],
        ]);

        $this->add([
            'name'       => 'submit[no]',
            'type'       => 'submit',
            'attributes' => [
                'name' => 'submit[no]',
                'value'       => 'Cancel',
                'data-action' => 'cancel',
            ],
        ]);
    }
}