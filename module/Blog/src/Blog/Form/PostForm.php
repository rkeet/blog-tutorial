<?php
namespace Blog\Form;

use Application\Form\AbstractForm;
use Zend\Form\Element;

class PostForm extends AbstractForm
{
    public function __construct($name = 'post-form', $options = [])
    {
        parent::__construct($name, $options);

        $this->addElements();
    }

    public function addElements()
    {
        $this->add([
            'name'     => 'id',
            'type'     => 'hidden',
            'required' => false,
        ]);

        $this->add([
            'name'     => 'title',
            'type'     => 'text',
            'required' => true,
            'options'  => [
                'label' => 'Title',
            ],
        ]);

        $this->add([
            'name'     => 'body',
            'type'     => 'textarea',
            'required' => true,
            'options'  => [
                'label' => 'Body',
            ],
        ]);

        $this->add([
            'name'       => 'submit',
            'type'       => 'submit',
            'attributes' => [
                'value' => 'Create post',
            ],
        ]);
    }
}