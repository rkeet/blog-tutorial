<?php

namespace Blog\InputFilter;

use Zend\InputFilter\InputFilter;
use Zend\InputFilter\Factory as InputFactory;

class PostInputFilter extends InputFilter
{
    public function __construct()
    {
        $factory = new InputFactory();

        $this->add(
            $factory->createInput([
                'name'        => 'id',
                'required'    => false,
                'allow_empty' => true,
                'filters'     => [
                    ['name' => 'Int'],
                ],
            ])
        );

        $this->add(
            $factory->createInput([
                'name'        => 'title',
                'required'    => true,
                'allow_empty' => false,
                'filters'     => [
                    ['name' => 'StripTags'],
                    ['name' => 'StringTrim'],
                ],
                'validators'  => [
                    [
                        'name'    => 'StringLength',
                        'options' => [
                            'min' => '3',
                            'max' => '128',
                        ],
                    ],
                ],
            ])
        );

        $this->add(
            $factory->createInput([
                'name'        => 'body',
                'required'    => true,
                'allow_empty' => false,
                'filters'     => [
                    ['name' => 'StripTags'],
                    ['name' => 'StringTrim'],
                ],
                'validators'  => [
                    [
                        'name'    => 'StringLength',
                        'options' => [
                            'min' => '10',
                        ],
                    ],
                ],
            ])
        );
    }
}